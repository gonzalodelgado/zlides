QUÉ ES
======

Uzebox es una consola retrominimalista hecha en casa (o mejor dicho que podemos hacer en casa)
utiliza el microcontrolador Atmega644 (de la línea que usan los Arduino) para hacer casi todo.

Es un excelente ejemplo de hardware libre. Su diseño está disponible bajo licencia GPL3, así que se puede modificar y extender como queramos. De hecho ya existen alternativas como Zuzebox que agrega puertos USB o EUzebox que funciona en televisores europeos con entrada RGB.

Uzebox es creada en 2008 por Alec Bourque quien siempre quiso crear su propia consola. Alec observaba que los proyectos de consolas caseras eran demasiado complejos, no manejaban muchos colores, y eran muy difíciles de programar.
Alec tomó muchas desiciones de diseño muy inteligentes:
* soportar una tarjeta SD de manera que los juegos se carguen desde ahí en lugar de estar grabando el juego en el microcontrolador cada vez
* usar el Atmega644 que tiene bastante memoria, muchas entradas y salidas y puede overclockearse
* hacer casi todo por software para poder tener mucha flexibilidad

Una vez que Alec lanzó el proyecto se formó una comunidad y se sumaron otras cosas importantes al proyecto:
* un emulador para probar los juegos en una computadora
* modos de video
* juegos!

COMO FUNCIONA
=============

como dije recién, la Uzebox hace casi todo por software, o sea que casi todo ocurre en el microcontrolador
un microcontrolador se puede ver como una pequeña computadora: tiene una CPU, memoria RAM y en el caso del Atmega644 tiene memoria de programa.
Esto es un poco distinto de una computadora común: cuando en nuestra computadora ejecutamos un programa, todos los datos y su código se cargan en memoria RAM. La arquitectura de esta línea de microcontroladores es distinta y guarda las variables en RAM y los datos constantes o estáticos en una memoria especial que se llama “memoria de programa”.
En la práctica esto significa que los gráficos, sonidos, mapas y demás datos estáticos de un juego van a la memoria del programa.

Retomando, el microcontrolador de la Uzebox se encarga de lo siguiente:

* lectura de controles
* generación de sonido
* generación de video
* lógica de juego

lo único que no hace el microcontrolador es la conversión de video RGB a NTSC, para eso la Uzebox cuenta con un chip extra, el AD75

SOFTWARE HACIENDO DE HARDWARE
============================

recién vimos todas las tareas del microcontrolador.
esto es bastante distinto de como las consolas de 8 bits de los 80 trabajaban,
ya que éstas tenían chips dedicados al procesamiento de gráficos y sonido, los cuales solo contaban con ciertas operaciones y definían los modos de video disponibles (un modo de vídeo es una configuración que dice que cantidad de pixeles hay en pantalla, cómo se organizan en memoria, etc.) y no se podían definir otros
Al encargarse de todo por software, Uzebox tiene la flexibilidad de soportar distintos modos de video, de hecho tiene casi 15 modos de video disponibles en este momento, además de que siempre podemos crear uno nuevo o adaptar uno existente si ninguno se ajusta a las necesidades de nuestro juego.

PROGRAMANDO JUEGOS
==================

Los juegos para Uzebox se programan en C (o en assembler si quieren :-P)
Uzebox trae un conjunto de funciones para el manejo de gráficos, texto y sonido muy buena.
A pesar de esto, programar juegos para Uzebox tiene muchas limitaciones:
* muy poca RAM
* la memoria de programa también es limitada
* no hay framebuffer (no se puede “dibujar”)
* la documentación está en inglés
