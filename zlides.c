/*
 * Copyright (c) 2016 Gonzalo Delgado
 *
 * This file is part of Zlides.
 *
 * Zlides is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Uzeferino is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Zlides. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Background art taken from Megatris
 * Pixel art TV set by Kieff (http://opengameart.org/)
 */

#include <stdbool.h>
#include <avr/io.h>
#include <stdlib.h>
#include <avr/pgmspace.h>
#include <uzebox.h>
#include "zlides.h"

#include "data/fonts.pic.inc"
#include "data/bg.inc"
#include "data/slideset1.inc"
#include "data/slideset2.inc"


static InputState GetInputState(InputState curr_state) {
        InputState result = {};
        result.last = curr_state.now;
        result.now = ReadJoypad(0);
        return result;
}

const int *slidemaps[] = {
        map_slide0,
        map_slide1,
        map_slide2,
        map_slide3,
        map_slide4,
        map_slide5,
};

int main() {
        InputState input_st = {};
	SetFontTable(fonts);
        ClearVram();
        u8 currSlide = 0;
	SetTileTable(background);
        DrawMap(0, 0, bg_main);
        
        SetTileTable(slideset1);
        while(1) {
                if (GetVsyncFlag()) {
                        ClearVsyncFlag();
                        input_st = GetInputState(input_st);
                        if (input_st.now != input_st.last) {
                                if (input_st.now & BTN_RIGHT && currSlide < ArrayCount(slidemaps) - 1) {
                                        currSlide++;
                                }
                                if (input_st.now & BTN_LEFT && currSlide > 0) {
                                        currSlide--;
                                }
                                if (currSlide > 2) {
                                        SetTileTable(slideset2);
                                }
                                if (currSlide < 3) {
                                        SetTileTable(slideset1);
                                }
                        }
                        DrawMap(3, 3, slidemaps[currSlide]); 
                }
        }

        return 0;
}
