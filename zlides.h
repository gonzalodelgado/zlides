/*
 * Copyright (c) 2016 Gonzalo Delgado
 *
 * This file is part of Zlides.
 *
 * Uzeferino is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Uzeferino is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Zlides. If not, see <http://www.gnu.org/licenses/>.
 */

#define SCREEN_WIDTH 240
#define SCREEN_HEIGHT (SCREEN_TILES_H << 3)     // multiplies by 8 == TILE_HEIGHT
#define SCREEN_TILES_WIDTH 240/TILE_WIDTH
#define SCREEN_TILES_HEIGHT 224/TILE_HEIGHT

#define ArrayCount(Array) (sizeof(Array) / sizeof((Array)[0])) 

typedef struct {
        u8 now, last;
} InputState;
